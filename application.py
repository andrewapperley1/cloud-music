__author__ = 'andrewapperley'

from app import create_app
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

app = create_app('dev')


if __name__ == '__main__':
    http_server = HTTPServer(WSGIContainer(app))

    http_server.bind(5000)
    http_server.start()
    IOLoop.instance().start()