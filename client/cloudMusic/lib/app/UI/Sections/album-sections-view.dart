library AlbumSectionView;

import "package:polymer/polymer.dart";
import "dart:async";
import "section-view.dart";
import "package:cloud_music/app/Networking/Music/Models/AlbumModel.dart";
import "package:cloud_music/app/Networking/Music/MusicModelFactory.dart";

@CustomTag("album-sections-view")
class AlbumSectionView extends SectionView {

  AlbumSectionView.created() : super.created();

  @observable
  List<AlbumModel> data;

  Future<bool> updateList() {
    Completer completion = new Completer();
    gotToAlbumSection(completion);
    return completion.future;
  }

  void handleListChange(e) {

  }

//  Gather Data
  void gotToAlbumSection(Completer completion) {
    MusicModelFactory factory = new MusicModelFactory();
    factory.albums().then((dynamic response) {
      if (response is List<AlbumModel>) {
        List<AlbumModel> temp_list = new List<AlbumModel>();
        for (AlbumModel album in response) {
          temp_list.add(album);
        }
        this.data = toObservable(temp_list);
        completion.complete(true);
      } else {
        completion.complete(null);
        // Display error on list or bring up alert or banner ...?
      }
    });
  }

}