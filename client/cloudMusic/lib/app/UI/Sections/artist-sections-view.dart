library ArtistSectionView;

import "package:polymer/polymer.dart";
import "dart:async";
import "section-view.dart";
import "package:cloud_music/app/Networking/Music/Models/ArtistModel.dart";
import "package:cloud_music/app/Networking/Music/MusicModelFactory.dart";

@CustomTag("artist-sections-view")
class ArtistSectionView extends SectionView {

  ArtistSectionView.created() : super.created();

  @observable
  List<ArtistModel> data;

  Future<bool> updateList() {
    Completer completion = new Completer();
    gotToArtistSection(completion);
    return completion.future;
  }

  void handleListChange(sender, d, _) {
    print(sender);
    print(d);
    print(_);
  }

//  Gather Data

  void gotToArtistSection(Completer completion) {
    MusicModelFactory factory = new MusicModelFactory();
    factory.artists().then((dynamic response) {
      if (response is List<ArtistModel>) {
        List<ArtistModel> temp_list = new List<ArtistModel>();
        for (ArtistModel artist in response) {
          temp_list.add(artist);
        }
        this.data = toObservable(temp_list);
        completion.complete(true);
      } else {
        completion.complete(false);
        // Send an error message along with the bool
      }
    });

  }

}