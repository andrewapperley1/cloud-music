library SectionsStatics;

enum SectionsHeaderSections {
  ArtistSection,
  AlbumSection,
  SongSection,
  PlaylistSection
}