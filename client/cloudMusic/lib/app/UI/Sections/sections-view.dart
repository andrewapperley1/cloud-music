import "package:polymer/polymer.dart";
import "dart:html";
import "package:paper_elements/paper_spinner.dart";
import "package:paper_elements/paper_toast.dart";
import "artist-sections-view.dart";
import "album-sections-view.dart";
import "song-sections-view.dart";
import "playlist-sections-view.dart";

@CustomTag("sections-view")
class SectionsView extends PolymerElement {

  SectionsView.created() : super.created();

  @observable
  int currentSection = 0;

  void ready() {
    goToSection(currentSection);
  }

  void goToSection(int section) {
    PaperSpinner loader = (shadowRoot.querySelector("#loading-spinner") as PaperSpinner);
    loader.active = true;
    switch(section) {
      case 0:
        ArtistSectionView artistSection = (shadowRoot.querySelector("#artist-section") as ArtistSectionView);
        artistSection.updateList().then((bool complete) {
          if (!complete) {
            _showErrorMessage();
          }
          currentSection = section;
          loader.active = false;
        });
      break;
      case 1:
        AlbumSectionView albumSection = (shadowRoot.querySelector("#album-section") as AlbumSectionView);
        albumSection.updateList().then((bool complete) {
          if (!complete) {
            _showErrorMessage();
          }
          currentSection = section;
          loader.active = false;
        });
        break;
      case 2:
        SongSectionView songSection = (shadowRoot.querySelector("#song-section") as SongSectionView);
        songSection.updateList().then((bool complete) {
          if (!complete) {
            _showErrorMessage();
          }
          currentSection = section;
          loader.active = false;
        });
        break;
      case 3:
        PlaylistSectionView playlistSection = (shadowRoot.querySelector("#playlist-section") as PlaylistSectionView);
        playlistSection.updateList().then((bool complete) {
          if (!complete) {
            _showErrorMessage();
          }
          currentSection = section;
          loader.active = false;
        });
        break;
    }

  }

  void _showErrorMessage() {
    PaperToast errorToast = (querySelector("#general-messages") as PaperToast);
    errorToast.text = "There was an error.";
    errorToast.show();
  }

}