library SongSectionView;

import "package:polymer/polymer.dart";
import "dart:html";
import "dart:async";
import "section-view.dart";
import "package:cloud_music/app/Networking/Music/Models/SongModel.dart";
import "package:cloud_music/app/Networking/Music/MusicModelFactory.dart";
import "package:cloud_music/app/UI/Now-Playing/now-playing.dart";

@CustomTag("song-sections-view")
class SongSectionView extends SectionView {

  NowPlaying _nowPlaying = null;

  SongSectionView.created() : super.created();

  @observable
  List<SongModel> data;

  Future<bool> updateList() {
    Completer completion = new Completer();
    gotToSongSection(completion);
    return completion.future;
  }

  void handleListChange(e) {
    if (_nowPlaying == null) {
      _nowPlaying = querySelector("#now-playing") as NowPlaying;
    }
    _nowPlaying.setCurrentSong(this.selection);
  }

//  Gather Data
  void gotToSongSection(Completer completion) {
    MusicModelFactory factory = new MusicModelFactory();
    factory.songs().then((dynamic response) {
      if (response is List<SongModel>) {
        List<SongModel> temp_list = new List<SongModel>();
        for (SongModel song in response) {
          temp_list.add(song);
        }
        this.data = toObservable(temp_list);
        completion.complete(true);
      } else {
        completion.complete(false);
        // Display error on list or bring up alert or banner ...?
      }
    });
  }

}