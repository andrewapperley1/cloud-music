library PlaylistSectionView;

import "package:polymer/polymer.dart";
import "dart:async";
import "section-view.dart";
import "package:cloud_music/app/Networking/Music/Models/PlaylistModel.dart";
import "package:cloud_music/app/Networking/Music/MusicModelFactory.dart";

@CustomTag("playlist-sections-view")
class PlaylistSectionView extends SectionView {

  PlaylistSectionView.created() : super.created();

  @observable
  List<PlaylistModel> data;

  Future<bool> updateList() {
    Completer completion = new Completer();
    gotToPlaylistSection(completion);
    return completion.future;
  }

  void handleListChange(e) {

  }

//  Gather Data

  void gotToPlaylistSection(Completer completion) {
    MusicModelFactory factory = new MusicModelFactory();
    factory.playlists().then((dynamic response) {
      if (response is List<PlaylistModel>) {
        List<PlaylistModel> temp_list = new List<PlaylistModel>();
        for (PlaylistModel playlist in response) {
          temp_list.add(playlist);
        }
        this.data = toObservable(temp_list);
        completion.complete(true);
      } else {
        completion.completeError(null);
        // Display error on list or bring up alert or banner ...?
      }
    });
  }

}