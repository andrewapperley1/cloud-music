import "dart:html";
import "dart:async";
import "package:polymer/polymer.dart";
import "package:cloud_music/app/Networking/Music/Models/SongModel.dart";

@CustomTag("now-playing")
class NowPlaying extends PolymerElement {

  @observable
  SongModel currentSong;

  @observable
  Map<String, String> shortInfo = _resetShortInfo();

  @observable
  String playStateIcon = "play";

  bool _playState = false;

  AudioElement audio = null;

  NowPlaying.created() : super.created();

  void setCurrentSong(SongModel song) {
    if (this.audio == null) {
      this.audio = shadowRoot.query("#playingSong");
    }
    this.currentSong = song;
    if (song == null) {
      shortInfo = _resetShortInfo();
      this.playStateIcon = "play";
      _playState = false;
      shadowRoot.querySelector("#now-playing-album-art").classes.add("semi-hidden");
    } else {
      _prepareShortInfo();
      this.playStateIcon = "pause";
      _playState = true;
      Future.delayed(new Duration(milliseconds:500), (_){
        audio.play();
      });
    }
  }

  static ObservableMap _resetShortInfo() {
    return toObservable({
        "title": "",
        "artist_name": "Select a song to Play",
        "album_title": "",
        "album_image_url": ""
    });
  }

  void _prepareShortInfo() {
    shadowRoot.querySelector("#now-playing-album-art").classes.remove("semi-hidden");
    shortInfo["title"] = (currentSong.title.length > 20) ? currentSong.title.substring(0, 17)+"..." : currentSong.title;
    shortInfo["artist_name"] = (currentSong.artist_name.length > 20) ? currentSong.artist_name.substring(0, 17)+"..." : currentSong.artist_name;
    shortInfo["album_title"] = (currentSong.album_title.length > 20) ? currentSong.album_title.substring(0, 17)+"..." : currentSong.album_title;
    shortInfo["album_image_url"] = currentSong.album_image_url;
  }

  void playPrevious(Event e, var detail, Node sender) {
    print(e);
    print(sender);
    print(detail);
  }

  void playNext(Event e, var detail, Node sender) {
    print(e);
    print(sender);
    print(detail);
  }

  void togglePlayState() {
    _playState = !_playState;
    if (this.playStateIcon == "play") {
      this.playStateIcon = "pause";
      this.audio.play();
    } else {
      this.playStateIcon = "play";
      this.audio.pause();
    }
  }

}