library sectionsHeader;

import "dart:html";
import "package:polymer/polymer.dart";
import "package:paper_elements/paper_tabs.dart";
import "package:cloud_music/app/Networking/Music/Models/AlbumModel.dart";
import "package:cloud_music/app/Networking/Music/Models/SongModel.dart";
import "package:cloud_music/app/Networking/Music/Models/PlaylistModel.dart";
import "package:cloud_music/app/Networking/Music/Models/SongAlbumModel.dart";
import "package:cloud_music/app/UI/Sections/sections-view.dart";
import "package:cloud_music/app/UI/Sections/sections-statics.dart";

@CustomTag("sections-header")
class SectionsHeader extends PolymerElement {

  SectionsHeader.created() : super.created();

  @observable
  String top_bar_title = "CloudMusic";

  void goToSectionWithClick(Event sender, d, _) {
    if (d['isSelected']) {
      PaperTabs tabs = (sender.target as PaperTabs);
      goToSectionWithSection(tabs.selected);
    }
  }

  void goToSectionWithSection(int section) {
    SectionsView mainSection = (querySelector("#main-section") as SectionsView);
    mainSection.goToSection(section);
  }
//
//
//  void goToInnerAlbumSection() {
//    MusicModelFactory factory = new MusicModelFactory();
//    factory.albumByID("13").then((dynamic response) {
//      if (response is SongAlbumModel) {
//        List<SongModel> dataList = (response as SongAlbumModel).songs;
//        (response as SongAlbumModel).length = dataList.length;
//        response.songs = [];
//        List<SongAlbumModel> headerList = new List<SongAlbumModel>();
//        headerList.add(response);
//        SectionsView section = (querySelector("#main-section") as SectionsView);
//        section.refreshList(dataList, headerList);
//      } else {
//        // Display error on list or bring up alert or banner ...?
//      }
//    });
//  }
}