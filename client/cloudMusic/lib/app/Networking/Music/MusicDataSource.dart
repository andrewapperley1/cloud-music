library MusicDataSource;
import "package:cloud_music/app/Networking/BaseNetworking/BaseDataSource.dart";
import "dart:async";

class MusicDataSource extends BaseDataSource {

  static final String  kMusicPlaylistIDReplace = "<playlistID>";
  static final String  kMusicArtistNameReplace = "<artistName>";
  static final String  kMusicAlbumIdReplace = "<albumId>";

  static final String  kMusicPlaylistsEndpoint = "music/playlists";
  static final String  kMusicPlaylistEndpoint = "music/playlist/"+kMusicPlaylistIDReplace;
  static final String  kMusicPlaylistSongsEndpoint = "music/playlist/"+kMusicPlaylistIDReplace+"/songs";
  static final String  kMusicArtistEndpoint = "music/artist/"+kMusicArtistNameReplace;
  static final String  kMusicArtistsEndpoint = "music/artists";
  static final String  kMusicSongsEndpoint = "music/songs";
  static final String  kMusicAlbumsEndpoint = "music/albums";
  static final String  kMusicAlbumEndpoint = "music/album/"+kMusicAlbumIdReplace;

  Future<Map> playlists() {
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), kMusicPlaylistsEndpoint);
  }

  Future<Map> playlistByID(String playlistID) {
    String endpoint = kMusicPlaylistEndpoint;
    endpoint = endpoint.replaceAll(new RegExp(r''+kMusicPlaylistIDReplace), playlistID);
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), endpoint);
  }

  Future<Map> playlistsSongByID(String playlistID) {
    String endpoint = kMusicPlaylistSongsEndpoint;
    endpoint = endpoint.replaceAll(new RegExp(r''+kMusicPlaylistIDReplace), playlistID);
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), endpoint);
  }

  Future<Map> artistByName(String artistName) {
    String endpoint = kMusicArtistEndpoint;
    endpoint = endpoint.replaceAll(new RegExp(r''+kMusicArtistNameReplace), artistName);
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), endpoint);
  }

  Future<Map> artists() {
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), kMusicArtistsEndpoint);
  }

  Future<Map> songs() {
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), kMusicSongsEndpoint);
  }

  Future<Map> albums() {
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), kMusicAlbumsEndpoint);
  }

  Future<Map> albumByID(String albumID) {
    String endpoint = kMusicAlbumEndpoint;
    endpoint = endpoint.replaceAll(new RegExp(r''+kMusicAlbumIdReplace), albumID);
    return super.makeAPIRequest(NetworkingRequestType.GET, new Map(), endpoint);
  }

}