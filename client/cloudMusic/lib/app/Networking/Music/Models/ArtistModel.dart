library ArtistModel;

import 'package:polymer/polymer.dart';
import "package:cloud_music/app/Configs/mainConfig.dart" show config, appRunMode;

final String kArtistsKey = "artists";
final String kArtistKey = "artist";
final String kArtistModelIDKey = "id";
final String kArtistModelNameKey = "name";
final String kArtistModelArtistImageURLKey = "artist_image_url";
final String kArtistModelAlbumCountKey = "album_count";
final String kArtistModelSongCountKey = "song_count";

class ArtistModel extends Object with Observable {
//  int id;

  @observable
    String templateType = "artistTemplate";
  @observable
    String name;
  @observable
    String artist_image_url;
  @observable
    int album_count;
  @observable
    int song_count;

  ArtistModel(this.name, this.artist_image_url, this.album_count, this.song_count);

  ArtistModel.fromJSON(Map jsonObject):
//    id = jsonObject["id"],
    this.name = jsonObject[kArtistModelNameKey],
    this.artist_image_url = config[appRunMode]["baseServerURL"] + jsonObject[kArtistModelArtistImageURLKey],
    this.album_count = jsonObject[kArtistModelAlbumCountKey],
    this.song_count = jsonObject[kArtistModelSongCountKey];
}