library SongModel;

import 'package:polymer/polymer.dart';
import "package:cloud_music/app/Configs/mainConfig.dart" show config, appRunMode;

final String kSongKey = "song";
final String kSongsKey = "songs";
final String kSongModelIDKey = "id";
final String kSongModelTitleKey = "title";
final String kSongModelAlbumTitleKey = "album_title";
final String kSongModelArtistNameKey = "artist_name";
final String kSongModelAlbumImageIDKey = "album_image_url";
final String kSongModelLengthKey = "length";


class SongModel extends Object with Observable {

  @observable
    String templateType = "songTemplate";

  @observable
    int id;

  @observable
    String title;

  @observable
    String album_title;

  @observable
    String artist_name;

  @observable
    String album_image_url;

  @observable
    int length;

  SongModel.fromJSON(Map jsonObject):
    id = jsonObject[kSongModelIDKey],
    title = jsonObject[kSongModelTitleKey],
    album_title = jsonObject[kSongModelAlbumTitleKey],
    artist_name = jsonObject[kSongModelArtistNameKey],
    album_image_url = config[appRunMode]["baseServerURL"] + jsonObject[kSongModelAlbumImageIDKey],
    length = jsonObject[kSongModelLengthKey];
}