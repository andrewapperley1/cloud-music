library SongAlbumModel;

import "AlbumModel.dart";
import "SongModel.dart";
import 'package:polymer/polymer.dart';

final String kAlbumModelSongsKey = "songs";

class SongAlbumModel extends AlbumModel {

  @observable
  String templateType = "innerAlbumTemplate";

  @observable
    List<SongModel> songs;

  @observable
    int length;

  SongAlbumModel.fromJSON(Map jsonObject):
    super.fromJSON(jsonObject),
    this.songs = _generateSongList(jsonObject[kAlbumModelSongsKey]);

  static _generateSongList(List jsonList) {
    List<SongModel> _songs = new List<SongModel>();
    for (Map songJSON in jsonList) {
      _songs.add(new SongModel.fromJSON(songJSON));
    }
    return _songs;
  }

}