library AlbumModel;

import "ArtistModel.dart";
import 'package:polymer/polymer.dart';
import "package:cloud_music/app/Configs/mainConfig.dart" show config, appRunMode;

final String kAlbumKey = "album";
final String kAlbumsKey = "albums";
final String kAlbumModelIDKey = "id";
final String kAlbumModelTitleKey = "title";
final String kAlbumModelYearKey = "year";
final String kAlbumModelAlbumImageURLKey = "album_image_url";
final String kAlbumModelLengthKey = "length";
final String kAlbumModelSongCountKey = "song_count";

class AlbumModel extends Object with Observable {

  @observable
    String templateType = "albumTemplate";
  @observable
    int id;
  @observable
    String title;
  @observable
    int year;
  @observable
    String album_image_url;
  @observable
    int length;
  @observable
    int song_count;

  @observable
    ArtistModel artist;

  AlbumModel(this.id, this.title, this.year, this.album_image_url, this.length, this.song_count, this.artist);

  AlbumModel.fromJSON(Map jsonObject):
    id = jsonObject[kAlbumModelIDKey],
    title = jsonObject[kAlbumModelTitleKey],
    year = jsonObject[kAlbumModelYearKey],
    album_image_url = config[appRunMode]["baseServerURL"] + jsonObject[kAlbumModelAlbumImageURLKey],
    length = jsonObject[kAlbumModelLengthKey],
    song_count = jsonObject[kAlbumModelSongCountKey],
    artist = new ArtistModel.fromJSON(jsonObject[kArtistKey]);
}