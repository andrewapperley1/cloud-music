library PlaylistModel;

import 'package:polymer/polymer.dart';

final String kPlayListModelIDKey = "id";
final String kPlayListModelNameKey = "name";
final String kPlayListModelLengthKey = "length";
final String kPlayListModelSongCountKey = "song_count";
final String kPlayListsKey = "playlists";
final String kPlayListKey = "playlist";
final String kPlayListSongsKey = "playlist_songs";

class PlaylistModel extends Object with Observable {

  @observable
    String templateType = "playlistTemplate";

  @observable
    int id;

  @observable
    String name;

  @observable
    int length;

  @observable
    int song_count;

  PlaylistModel.fromJSON(Map jsonObject):
    id = jsonObject["id"],
    name = jsonObject["name"],
    length = jsonObject["length"],
    song_count = jsonObject["song_count"];
}