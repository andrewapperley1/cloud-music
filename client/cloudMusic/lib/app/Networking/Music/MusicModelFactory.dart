library MusicModelFactory;

import "MusicDataSource.dart";
import "package:cloud_music/app/Networking/BaseNetworking/BaseNetworkingStatics.dart";
import "Models/PlaylistModel.dart";
import "Models/AlbumModel.dart";
import "Models/ArtistModel.dart";
import "Models/SongModel.dart";
import "Models/SongAlbumModel.dart";
import "package:cloud_music/app/Networking/BaseNetworking/BaseErrorModel.dart";
import "dart:async";

class MusicModelFactory {

  MusicDataSource _datasource;

  MusicModelFactory() {
    _datasource = new MusicDataSource();
  }

  Future<dynamic> playlists() {
    return _datasource.playlists().then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        List<PlaylistModel> playlists = new List<PlaylistModel>();
        for (Map playlist in response[kPlayListsKey]) {
          playlists.add(new PlaylistModel.fromJSON(playlist));
        }
        return playlists;
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> playlistByID(String playlistID) {
    return _datasource.playlistByID(playlistID).then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        return new PlaylistModel.fromJSON(response[kPlayListKey]);
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> playlistSongsByID(String playlistID) {
    return _datasource.playlistsSongByID(playlistID).then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        List<SongModel> songs = new List<SongModel>();
        for (Map song in response[kPlayListSongsKey]) {
          songs.add(new SongModel.fromJSON(song));
        }
        return songs;
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> artistByName(String artistName) {
    return _datasource.artistByName(artistName).then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        return new ArtistModel.fromJSON(response[kArtistKey]);
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> artists() {
    return _datasource.artists().then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        List<ArtistModel> artists = new List<ArtistModel>();
        for (Map artist in response[kArtistsKey]) {
          artists.add(new ArtistModel.fromJSON(artist));
        }
        return artists;
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> songs() {
    return _datasource.songs().then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        List<SongModel> songs = new List<SongModel>();
        for (Map song in response[kSongsKey]) {
          songs.add(new SongModel.fromJSON(song));
        }
        return songs;
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> albums() {
    return _datasource.albums().then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        List<AlbumModel> albums = new List<AlbumModel>();
        for (Map album in response[kAlbumsKey]) {
          albums.add(new AlbumModel.fromJSON(album));
        }
        return albums;
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

  Future<dynamic> albumByID(String albumID) {
    return _datasource.albumByID(albumID).then((Map response) {
      if (response[kBaseDataSourceStatusKey]) {
        return new SongAlbumModel.fromJSON(response[kAlbumKey]);
      } else {
        return new BaseErrorModel(response[kBaseDataSourceHTTPCodeKey], response[kBaseDataSourceMessageKey]);
      }
    });
  }

}