library BaseNetworkingStatics;
final String kBaseDataSourceMessageKey = "message";
final String kBaseDataSourceHTTPCodeKey = "HTTP_CODE";
final String kBaseDataSourceStatusKey = "status";
final String kBaseDataSourceAPIKey = "api_key";
final String kBaseDataSourceAccountIDKey = "account_id";
final String kBaseDataSourceSessionIDKey = "session_id";