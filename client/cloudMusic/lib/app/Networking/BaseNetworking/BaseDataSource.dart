library BaseDataSource;
import "package:cloud_music/app/Configs/mainConfig.dart" show config, appRunMode;
import "dart:async";
import "dart:html";
import "dart:convert";
import "BaseNetworkingStatics.dart";
import "package:cloud_music/app/Networking/Authentication/AuthenticationMediator.dart";

enum NetworkingRequestType {
  GET, POST
}

class BaseDataSource {

  static final String baseServerURL = config[appRunMode]["baseServerURL"];

  Future<Map> makeAPIRequest(NetworkingRequestType type, Map params, String endpoint) {
    AuthenticationMediator mediator = new AuthenticationMediator();
    params[kBaseDataSourceAccountIDKey] = mediator.currentUser.account_id;
    params[kBaseDataSourceSessionIDKey] = mediator.currentUser.session_id;
    return makeRequest(type, params, endpoint);
  }

  Future<Map> makeRequest(NetworkingRequestType type, Map params, String endpoint) {

    Future<Map> completion;
    params[kBaseDataSourceAPIKey] = config[appRunMode]["api_key"];

    switch (type) {
      case NetworkingRequestType.GET:
        completion = _makeGetRequest(endpoint, params);
        break;

      case NetworkingRequestType.POST:
        completion = _makePostRequest(endpoint, params);
        break;
    }

    return completion;
  }

  Future<Map> _makeGetRequest(String endpoint, Map params) {
    final uri = new Uri(path: "${baseServerURL}${endpoint}",
    queryParameters: params);
    return HttpRequest.getString("${uri}").then(JSON.decode);
  }

  Future<Map> _makePostRequest(String endpoint, Map params) {
    final Map finalParams = {"params": JSON.encode(params)};
    return HttpRequest.postFormData("${baseServerURL}${endpoint}", finalParams).then((HttpRequest response) {
      return JSON.decode(response.response);
    });
  }

}