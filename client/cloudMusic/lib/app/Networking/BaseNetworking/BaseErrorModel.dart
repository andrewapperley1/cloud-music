library BaseErrorModel;


class BaseErrorModel {

  int HTTPCode;
  String error;

  BaseErrorModel(int code, String errorString):
    HTTPCode = code,
    error = errorString;

}