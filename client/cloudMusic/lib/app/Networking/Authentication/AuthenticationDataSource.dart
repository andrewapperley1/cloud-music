library AuthenticationDataSource;
import "package:cloud_music/app/Networking/BaseNetworking/BaseDataSource.dart";
import "dart:async";

class AuthenticationDataSource extends BaseDataSource {

  static final String  kAuthenticationCreateAccountEndpoint = "auth/account/create";
  static final String  kAuthenticationFinalizeAccountEndpoint = "auth/account/finalize";
  static final String  kAuthenticationSignonAccountEndpoint = "auth/account/signOn";
  static final String  kAuthenticationAuthenticateAccountEndpoint = "auth/account/authenticate";
  static final String  kAuthenticationSignoutAccountEndpoint = "auth/account/logout";


  Future<Map> createAccount() {
    return super.makeRequest(NetworkingRequestType.POST, new Map(), kAuthenticationCreateAccountEndpoint);
  }

  Future<Map> finalizeAccount(Map params) {
    return super.makeRequest(NetworkingRequestType.POST, params, kAuthenticationFinalizeAccountEndpoint);
  }

  Future<Map> signIntoAccount(Map params) {
    return super.makeRequest(NetworkingRequestType.POST, params, kAuthenticationSignonAccountEndpoint);
  }

  Future<Map> authenticateAccount(Map params) {
    return super.makeRequest(NetworkingRequestType.POST, params, kAuthenticationAuthenticateAccountEndpoint);
  }

  Future<Map> signOutOfAccount(Map params) {
    return super.makeRequest(NetworkingRequestType.POST, params, kAuthenticationSignoutAccountEndpoint);
  }

}