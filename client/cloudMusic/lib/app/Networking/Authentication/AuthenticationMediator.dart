library AuthenticationMediator;

import "dart:async";
import "AuthenticationDataSource.dart";
import "AuthenticationUserModel.dart";
import "dart:html";
import "dart:convert";
import "package:cloud_music/app/Networking/BaseNetworking/BaseNetworkingStatics.dart";
import "package:cloud_music/app/Configs/mainConfig.dart" show config, appRunMode;
import "dart:js";


/**
 * This Class is a singleton.
 * It allows you to control authentication flow and access the current user for API requests
 * */
class AuthenticationMediator {

  static String kAuthenticationResponseObjectKeyAccountId = "account_id";
  static String kAuthenticationResponseObjectKeySessionId = "session_id";
  static String kAuthenticationResponseObjectKeyPasswordHash = "password_hash";
  static String kAuthenticationResponseObjectKeyOffset = "offset";

  static String kAuthenticationCurrentUserKey = "AuthenticationCurrentUser";

  Storage localStorage = window.localStorage;
  AuthenticationUserModel _currentUser;

  static final AuthenticationMediator _singleton = new AuthenticationMediator._internal();

  factory AuthenticationMediator() {
    return _singleton;
  }

  AuthenticationMediator._internal() {
    this._currentUser = this.currentUser;
  }

  AuthenticationUserModel get currentUser {
    if (_currentUser == null) {
      String userString = this.localStorage[kAuthenticationCurrentUserKey];
      if (userString != null) {
        Map jsonUser = JSON.decode(userString);
        _currentUser = new AuthenticationUserModel.fromJSON(jsonUser);
      } else {
        _currentUser = new AuthenticationUserModel();
      }
    }
    return _currentUser;
  }

  bool loggedIn() {
    return this._currentUser.signed_in;
  }

  Future<bool> login() {
    Completer<bool> completion = new Completer();
    AuthenticationDataSource authDataSource = new AuthenticationDataSource();

    void _loginSequence() {
      if (!this._currentUser.finalized) {
        if (this._currentUser.account_id == "") {
          authDataSource.createAccount().then((Map response) {
            if (response[kBaseDataSourceStatusKey]) {
              this._currentUser.account_id = response[kAuthenticationResponseObjectKeyAccountId];
              this._currentUser.session_id = response[kAuthenticationResponseObjectKeySessionId];
              this._currentUser.offset = response[kAuthenticationResponseObjectKeyOffset];
              saveCurrentUser();
              _loginSequence();
            } else {
              completion.complete(false);
            }
          });
        } else {
          Map params = {
              kAuthenticationResponseObjectKeySessionId: this._currentUser.session_id,
              kAuthenticationResponseObjectKeyPasswordHash: _generatePassword()
          };
          authDataSource.finalizeAccount(params).then((Map response) {
            if (response[kBaseDataSourceStatusKey]) {
              this._currentUser.session_id = "";
              this._currentUser.finalized = true;
              saveCurrentUser();
              _loginSequence();
            } else {
              completion.complete(false);
            }
          });
        }

      } else {
        if (this._currentUser.session_id == "") {
          Map params = {
              kAuthenticationResponseObjectKeyAccountId: this._currentUser.account_id
          };
          authDataSource.signIntoAccount(params).then((Map response) {
            if (response[kBaseDataSourceStatusKey]) {
              this._currentUser.session_id = response[kAuthenticationResponseObjectKeySessionId];
              this._currentUser.offset = response[kAuthenticationResponseObjectKeyOffset];
              saveCurrentUser();
              _loginSequence();
            } else {
              completion.complete(false);
            }
          });
        } else {
          Map params = {
              kAuthenticationResponseObjectKeySessionId: this._currentUser.session_id,
              kAuthenticationResponseObjectKeyPasswordHash: _generatePassword()
          };
          authDataSource.authenticateAccount(params).then((Map response) {
            if (response[kBaseDataSourceStatusKey]) {
              if (this._currentUser.sign_in()) {
                saveCurrentUser();
                completion.complete(true);
              } else {
                _loginSequence();
              }
            } else {
              completion.complete(false);
            }
          });
        }
      }
    }
    if (this._currentUser.isValid()) {
      completion.complete(true);
    } else {
      Future _logoutCompletion = logout();
      _logoutCompletion.then((bool success) {
        _loginSequence();
      });
    }

    return completion.future;
  }

  Future<bool> logout() {
    Completer<bool> completion = new Completer();
    if (!this._currentUser.isComplete()) {
      this._currentUser.resetUser();
      saveCurrentUser();
      completion.complete(true);
      return completion.future;
    }
    AuthenticationDataSource authDataSource = new AuthenticationDataSource();
    Map params = {
        kAuthenticationResponseObjectKeySessionId: this._currentUser.session_id,
        kAuthenticationResponseObjectKeyPasswordHash: _generatePassword()
    };
    authDataSource.signOutOfAccount(params).then((Map response) {
      this._currentUser.resetUser();
      saveCurrentUser();
      completion.complete(true);
    });
    return completion.future;
  }

  String _generatePassword() {
    String secret_key = config[appRunMode]["secret_key"];
    int offset = this._currentUser.offset;
    int chunk_amount = config[appRunMode]["sectet_key_chunk_amount"];
    String secret_chunk;
    if (offset + chunk_amount > secret_key.length) {
      int left_over_offset = (offset + chunk_amount) - secret_key.length;
      secret_chunk = secret_key.substring(offset);
      secret_chunk += secret_key.substring(0, (offset + chunk_amount) - secret_key.length);
    } else {
      secret_chunk = secret_key.substring(offset, offset+chunk_amount);
    }

    secret_chunk = secret_chunk.codeUnits.map((e) {
      return e.toRadixString(2).padLeft(8, "0");
    }).join();
    var cryptoJS = context['CryptoJS'];
    var secret = cryptoJS.callMethod('SHA256', ["${secret_chunk}${this._currentUser.account_id}${this._currentUser.session_id}"]);
    return secret.toString();
  }

  void saveCurrentUser() {
    Map jsonUser = this._currentUser.toJSONObject();
    this.localStorage[kAuthenticationCurrentUserKey] = JSON.encode(jsonUser);
  }

}