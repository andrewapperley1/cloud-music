library AuthenticationUserModel;

class AuthenticationUserModel {

  static final kMillisecondsInADay = 86400000;

  String account_id;
  String session_id;
  int offset;
  bool finalized;
  bool signed_in;
  int creation_date;

  AuthenticationUserModel() {
    resetUser();
  }

  AuthenticationUserModel.fromJSON(Map jsonObject):
    account_id = jsonObject["account_id"],
    session_id = jsonObject["session_id"],
    offset = jsonObject["offset"],
    finalized = jsonObject["finalized"],
    signed_in = jsonObject["signed_in"],
    creation_date = jsonObject["creation_date"];

  bool sign_in() {
    if (this.finalized == false) {
      return false;
    }
    this.signed_in = true;
    DateTime now = new DateTime.now();
    this.creation_date = now.millisecondsSinceEpoch + kMillisecondsInADay;
    return true;
  }

  bool isValid() {
    if (this.creation_date == null) {
      return false;
    }
    DateTime now = new DateTime.now();
    DateTime modelExpiryTime = new DateTime.fromMillisecondsSinceEpoch(this.creation_date);
    return now.isBefore(modelExpiryTime);
  }

  void resetUser() {
    this.account_id = "";
    this.session_id = "";
    this.offset = -1;
    this.finalized = false;
    this.signed_in = false;
    this.creation_date = null;
  }

  bool isComplete() {
    return (this.account_id != null && this.session_id != null && this.offset != -1);
  }

  String toJSONString() {
    return "{'account_id': $account_id,"+
    "'session_id': $session_id,"+
    "'offset': $offset,"+
    "'finalized': $finalized,"+
    "'signed_in': $signed_in,"+
    "'creation_date': $creation_date"+
    "}";
  }

  Map toJSONObject() {
    return {
        'account_id': this.account_id,
        'session_id': this.session_id,
        'offset': this.offset,
        'finalized': this.finalized,
        'signed_in': this.signed_in,
        'creation_date': this.creation_date
    };
  }

}