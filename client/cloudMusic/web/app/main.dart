import "package:polymer/polymer.dart";
import "package:cloud_music/app/Networking/Authentication/AuthenticationMediator.dart";

void main() {
//  // Authenticate with server then init polymer for the UI
  AuthenticationMediator mediator = new AuthenticationMediator();
  mediator.login().then((bool success) {
    print("authenticated");
//    if (success) {
      initPolymer().run(() {
        print("polymer init");
        // code here works most of the time
        Polymer.onReady.then((_) {
          print("polymer ready");
          // some things must wait until onReady callback is called
        });
      });
//    } else {
//      // Show Error, or maybe show error above after initializing polymer but don't let the app continue. Toast?
//    }
  });
}