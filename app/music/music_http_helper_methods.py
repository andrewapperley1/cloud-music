__author__ = 'andrewapperley'

import urllib
import urllib2
import json
from flask import current_app

ARTIST_BASE_URL = "https://itunes.apple.com/search?"
ALBUM_ART_BASE_URL = "https://itunes.apple.com/lookup?"
ARTIST_ART_BASE_URL = "http://developer.echonest.com/api/v4/artist/images?"


def get_album_art_save(artist_name, album_name, local_url):
    params = urllib.urlencode({'media': 'music', 'entity': 'musicArtist', 'attribute': 'artistTerm', 'limit': 1, 'term': artist_name})
    response = urllib2.urlopen(ARTIST_BASE_URL+params)
    if response:
        artist_JSON = json.load(response)
        params = urllib.urlencode({'entity': 'album', 'id': artist_JSON['results'][0]['artistId']})
        response = urllib2.urlopen(ALBUM_ART_BASE_URL+params)
        if response:
            album_JSON = json.load(response)
            album_art_url = None
            album_name = str(album_name).translate(None, '!@#$%^&*()_-+=,<.>/?;:[{]}\|~`')
            album_name_list = album_name.lower().rsplit()
            for album in album_JSON['results'][1:]:
                result_album_name_list = str(album['collectionName']).translate(None, '!@#$%^&*()_-+=,<.>/?;:[{]}\|~`').lower().rsplit()
                if len(set(album_name_list) & set(result_album_name_list)) > 0:
                    album_art_url = album['artworkUrl100']
                    break

            if album_art_url:
                save_album_art(album_art_url, local_url)


def get_artist_art_save(artist_name, local_url):
    params = urllib.urlencode({'api_key': current_app.config['ECHO_NEST_API_KEY'], 'name': artist_name, 'format': 'json', 'results': 1, 'start': 0, 'license': 'unknown'})
    response = urllib2.urlopen(ARTIST_ART_BASE_URL + params)
    if response:
        artist_JSON = json.load(response)
        artist_image_url = artist_JSON['response']['images'][0]['url']
        save_album_art(artist_image_url, local_url)


def save_album_art(art_url, local_url):
    urllib.urlretrieve(art_url, local_url)