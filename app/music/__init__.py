__author__ = 'andrewapperley'

from flask import Blueprint

music = Blueprint('music', __name__)

from . import endpoints
from . import dashboard_endpoints