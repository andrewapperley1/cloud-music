__author__ = 'andrewapperley'

from .. import db
from sqlalchemy.orm import validates
import hashlib
from flask import current_app


class Artist(db.Model):
    __tablename__ = 'artists'
    #TODO change this to use an ID for primary and Index the name
    name = db.Column(db.String(100), primary_key=True)
    artist_image_id = db.Column(db.String(255))
    album_count = db.Column(db.Integer)
    song_count = db.Column(db.Integer)

    def __init__(self, name, artist_image_id):
        self.name = name
        self.artist_image_id = artist_image_id
        self.album_count = 0
        self.song_count = 0

    def __repr__(self):
        return '<Artist %s>' % self.name

#TODO change this to only return the relative path so the client can determine which server to connect to
    def to_json(self):
        return {
            'name': self.name,
            'artist_image_url': current_app.config['PUBLIC_MEDIA_FOLDER_URL']+"artists/"+self.artist_image_id+".jpg",
            'album_count': self.album_count,
            'song_count': self.song_count
        }

    def all_albums(self):
        _albums = Album.query.filter_by(artist_name=self.name).all()

        return_albums = []
        for album in _albums:
            _songs = Song.query.filter_by(album_id=album.id).all()
            return_songs = []
            for song in _songs:
                return_songs.append(song.to_json())
            _album = album.to_json()
            _album['songs'] = return_songs
            return_albums.append(_album)

        return {
            'name': self.name,
            'albums': return_albums
        }


class Album(db.Model):
    __tablename__ = 'albums'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), index=True)
    year = db.Column(db.Integer)
    length = db.Column(db.Integer)
    artist_name = db.Column(db.String(100), db.ForeignKey(Artist.name))
    artist = db.relationship('Artist', foreign_keys='Album.artist_name')
    album_image_id = db.Column(db.String(255))
    song_count = db.Column(db.Integer)

    @validates('artist_name')
    def update_artist_album_count(self, key, value):
        artist = Artist.query.filter_by(name=value).first()
        artist.album_count += 1
        return value

    def __init__(self, title, year, album_image_id, artist_name):
        self.id = None
        self.title = title
        self.year = year
        self.album_image_id = album_image_id
        self.artist_name = artist_name
        self.length = 0
        self.song_count = 0

    @classmethod
    def albumWithTag(cls, id3tag):
        album = Album(id3tag.album,
                      (id3tag.getBestDate() is not None) and id3tag.getBestDate().year or None,
                      hashlib.md5(id3tag.album).hexdigest(),
                      id3tag.artist)
        return album

    def __repr__(self):
        return '<Album %s, %s>' % self.title, self.artist_name

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title,
            'year': self.year,
            'album_image_url': current_app.config['PUBLIC_MEDIA_FOLDER_URL']+"albums/"+self.album_image_id+".jpg",
            'length': round(self.length / 60),
            'song_count': self.song_count,
            'artist': self.artist.to_json()
        }

    def all_songs(self):
        _album = self.to_json()
        _songs = Song.query.filter_by(album_id=self.id).all()
        returnSongs = []
        for _song in _songs:
            returnSongs.append(_song.to_json())
        _album['songs'] = returnSongs
        return _album


class Song(db.Model):
    __tablename__ = 'songs'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    album_id = db.Column(db.Integer, db.ForeignKey(Album.id))
    album = db.relationship('Album', foreign_keys='Song.album_id')
    length = db.Column(db.Integer)

    @validates('album_id')
    def update_album_song_count_and_length(self, key, value):
        album = Album.query.filter_by(id=value).first()
        album.song_count += 1
        album.artist.song_count += 1
        album.length += self.length
        return value

    def __init__(self, title, length, album_id):
        self.id = None
        self.title = title
        self.length = length
        self.album_id = album_id

    def __repr__(self):
        return '<Song %s>' % self.title

    def to_json(self):
        return {
            'id': self.id,
            'title': self.title,
            'album_title': self.album.title,
            'artist_name': self.album.artist_name,
            'album_image_url': current_app.config['PUBLIC_MEDIA_FOLDER_URL']+"albums/"+self.album.album_image_id+".jpg",
            'length': round(self.length / 60.0)
        }


class Playlist(db.Model):
    __tablename__ = 'playlists'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    length = db.Column(db.Integer)
    song_count = db.Column(db.Integer)

    def __init__(self, name):
        self.id = None
        self.name = name
        self.length = 0
        self.song_count = 0

    def __repr__(self):
        return '<Playlist %s>' % self.name

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'length': round(self.length / 60.0),
            'song_count': self.song_count
        }


class PlaylistSong(db.Model):
    __tablename__ = 'playlist_songs'
    id = db.Column(db.Integer, primary_key=True)
    song_id = db.Column(db.Integer, db.ForeignKey(Song.id))
    song = db.relationship('Song', foreign_keys='PlaylistSong.song_id')
    playlist_id = db.Column(db.Integer, db.ForeignKey(Playlist.id))
    playlist = db.relationship('Playlist', foreign_keys='PlaylistSong.playlist_id')

    def __init__(self, song_id, playlist_id):
        self.id = None
        self.song_id = song_id
        self.playlist_id = playlist_id

    def __repr__(self):
        return '<PlaylistSong song_id=%i, playlist_id=%i>' % self.song_id, self.playlist_id

    def to_json(self):
        return {
            'id': self.id,
            'song': self.song.to_json(),
            'playlist': self.playlist.to_json()
        }