__author__ = 'andrewapperley'

from flask import jsonify, request
from . import music, models, music_http_helper_methods
from flask.ext.login import login_required
import eyed3
import hashlib
import os
import shutil


@music.route('/dashboard/artists', methods=['GET'])
@login_required
def dashboard_music_artists():
    artists = models.Artist.query.order_by(models.Artist.name).all()
    albums = models.Album.query.with_entities(models.Album.artist_name, models.Album.album_image_id, models.Album.length).order_by(models.Album.artist_name).all()
    _artists = []
    for artist in artists:
        _artists.append({
            "artist": artist.to_json(),
            "albums": [a[1] for a in albums if a[0] == artist.name],
            "total_time": sum([a[2] for a in albums if a[0] == artist.name])
        })
    response = jsonify(    message="Artists",
                           status=True,
                           HTTP_CODE=200,
                           artists=_artists
                      )
    response.status_code = 200
    return response


@music.route('/dashboard/upload', methods=['POST'])
@login_required
def dashboard_music_upload():
    music_uploads = request.files
    temp_folder_str = os.path.join(os.path.dirname(__file__), os.pardir, "static/media/temp/"+hashlib.sha224(str(music_uploads["music[0]"].filename) + str(os.urandom(16))).hexdigest())
    final_folder_str = os.path.join(os.path.dirname(__file__), os.pardir, "static/media/private/music/")
    album_folder_str = os.path.join(os.path.dirname(__file__), os.pardir, "static/media/public/albums/")
    artist_folder_str = os.path.join(os.path.dirname(__file__), os.pardir, "static/media/public/artists/")
    os.makedirs(temp_folder_str)

    for song in music_uploads:
        artist_check = False
        album_check = False
        artist = None
        album = None
        song_object = music_uploads[song]
        song_object_path = temp_folder_str+"/"+song+".mp3"
        song_object.save(song_object_path)
        song_object.close()
        song_file = eyed3.load(song_object_path)
        try:
            artist_check = bool(models.Artist.query.filter_by(name=song_file.tag.artist).count())
            if artist_check is True:
                artist = models.Artist.query.filter_by(name=song_file.tag.artist).first()
                album_check = bool(models.Album.query.filter_by(artist_name=song_file.tag.artist, title=song_file.tag.album).count())
                if album_check is not True:
                    album = models.Album.albumWithTag(song_file.tag)
                    music_http_helper_methods.get_album_art_save(artist.name, album.title, album_folder_str+album.album_image_id+".jpg")
                    models.db.session.add(album)
                else:
                    album = models.Album.query.filter_by(title=song_file.tag.album).first()
            else:
                artist = models.Artist(song_file.tag.artist, hashlib.md5(song_file.tag.artist).hexdigest())
                models.db.session.add(artist)
                music_http_helper_methods.get_artist_art_save(artist.name, artist_folder_str+artist.artist_image_id+".jpg")
                album = models.Album.albumWithTag(song_file.tag)
                music_http_helper_methods.get_album_art_save(artist.name, album.title, album_folder_str+album.album_image_id+".jpg")
                models.db.session.add(album)
            models.db.session.commit()
        except Exception as e:
            print(e.message)
            raise ValueError("Album and Artist creation failed")

        try:
            _song = models.Song(song_file.tag.title, song_file.info.time_secs, album.id)
            models.db.session.add(_song)
            models.db.session.commit()
            shutil.copy2(song_object_path, final_folder_str+str(_song.id)+".mp3")
        except Exception as e:
            print(e.message)
            raise ValueError("Song creation failed")

    try:
        shutil.rmtree(temp_folder_str)
    except Exception as e:
        print(e.message)
        raise ValueError("Clean up failed")

    response = jsonify(    message="Song Upload Successful",
                           status=True,
                           HTTP_CODE=200
                      )
    response.status_code = 200
    return response
