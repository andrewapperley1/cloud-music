__author__ = 'andrewapperley'

from flask import jsonify, request
from . import music, models
from ..auth import functions as auth_functions

authentication_endpoint_blacklist = ["music.playlists", "music.playlist", "music.artist", "music.artists",
                                     "music.songs", "music.albums", "music.albumById"]


# Authenticate
@music.before_request
def before():
    if request.endpoint in authentication_endpoint_blacklist:
        if request.method == "GET":
            api_key = request.args["api_key"]
            account_id = request.args["account_id"]
            session_id = request.args["session_id"]
        else:
            api_key = request.get_json()["params"]["api_key"]
            account_id = request.get_json()["params"]["account_id"]
            session_id = request.get_json()["params"]["session_id"]
        if auth_functions.Auth().verify_api_session(session_id, account_id, api_key) is False:
            response = jsonify(     message="You are not authorized to access that resource",
                                    status=False,
                                    HTTP_CODE=401
                      )
            response.status_code = 401
            return response


@music.route('/playlists', methods=["GET"])
def playlists():

    _playlists = models.Playlist.query.order_by(models.Playlist.id).all()

    returnPlaylists = []
    for _playlist in _playlists:
        returnPlaylists.append(_playlist.to_json())

    response = jsonify(    message="Your playlists",
                           status=True,
                           HTTP_CODE=200,
                           playlists=returnPlaylists
                      )
    response.status_code = 200
    return response


@music.route('/playlist/<playlistID>', methods=["GET"])
def playlist(playlistID):

    _playlist = models.Playlist.query.filter_by(id=playlistID).first()
    if _playlist is not None:
        _playlist = _playlist.to_json()
    else:
        _playlist = {}

    response = jsonify(    message="Playlist "+playlistID,
                           status=True,
                           HTTP_CODE=200,
                           playlist=_playlist
                      )
    response.status_code = 200
    return response


@music.route('/playlist/<playlistID>/songs', methods=["GET"])
def playlist_songs(playlistID):

    _playlist_songs = models.PlaylistSong.query.filter_by(playlist_id=playlistID).all()
    if _playlist_songs is not None:
        _songs = []
        for song in _playlist_songs:
            _songs.append(song.to_json())
        response = jsonify(     message="Songs for playlist "+playlistID,
                                status=True,
                                HTTP_CODE=200,
                                playlist_songs=_songs
                            )
        response.status_code = 200
        return response


@music.route('/artist/<artistName>', methods=["GET"])
def artist(artistName):

    _artist = models.Artist.query.filter_by(name=artistName).first()
    if _artist is not None:
        _artist = _artist.all_albums()
    else:
        _artist = {}
    response = jsonify(    message=artistName,
                           status=True,
                           HTTP_CODE=200,
                           artist=_artist
                      )
    response.status_code = 200
    return response


@music.route('/artists', methods=["GET"])
def artists():
    _artists = models.Artist.query.order_by(models.Artist.name).all()
    returnArtists = []
    for _artist in _artists:
        returnArtists.append(_artist.to_json())


    response = jsonify(    message="All artists",
                           status=True,
                           HTTP_CODE=200,
                           artists=returnArtists
                      )
    response.status_code = 200
    return response


@music.route('/songs', methods=["GET"])
def songs():
    _songs = models.Song.query.order_by(models.Song.title).all()

    returnSongs = []
    for _song in _songs:
        returnSongs.append(_song.to_json())

    response = jsonify(    message="All songs",
                           status=True,
                           HTTP_CODE=200,
                           songs=returnSongs
                      )
    response.status_code = 200
    return response


@music.route('/albums', methods=["GET"])
def albums():
    _albums = models.Album.query.order_by(models.Album.title).all()

    returnAlbums = []
    for _album in _albums:
        returnAlbums.append(_album.to_json())

    response = jsonify(    message="All albums",
                           status=True,
                           HTTP_CODE=200,
                           albums=returnAlbums
                      )
    response.status_code = 200
    return response

@music.route('/album/<albumId>', methods=["GET"])
def albumById(albumId):
    _album = models.Album.query.filter_by(id=albumId).first()

    if _album is not None:
        _album = _album.all_songs()
    else:
        _album = {}

    response = jsonify(    message="Album "+albumId,
                           status=True,
                           HTTP_CODE=200,
                           album=_album
                      )
    response.status_code = 200
    return response