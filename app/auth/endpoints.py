__author__ = 'andrewapperley'

from . import auth, models, functions
from flask import jsonify, request, json
import uuid


@auth.route("/account/create", methods=["POST"])
def create_account():
    account_id = uuid.uuid4().hex
    account = models.ApiUser(account_id)
    models.db.session.add(account)
    models.db.session.commit()

    session_id = uuid.uuid4().hex
    offset = functions.Auth().create_session_offset()
    session = models.ApiUserSession(session_id, account_id, offset)
    models.db.session.add(session)
    models.db.session.commit()

    models.db.session.close()

    response = jsonify(    message="Account Creation Successfully",
                           status=True,
                           HTTP_CODE=200,
                           account_id=account_id,
                           session_id=session_id,
                           offset=offset
                      )
    response.status_code = 200
    return response


@auth.route("/account/finalize", methods=["POST"])
def finalize_account():
    req = json.loads(request.form["params"].encode('utf8'))
    session_id = req["session_id"]
    password_hash = str(req["password_hash"])
    response = None
    if session_id is not None and password_hash is not None:
        session = models.ApiUserSession.query.filter_by(id=session_id).first()
        if session is not None:
            session_password = functions.Auth().create_password_hash(session_id, session.offset_key, session.api_user_id)
            if password_hash == session_password:
                # Finalize Account & Delete Session
                account = models.ApiUser.query.filter_by(id=session.api_user_id).first()
                account.state = 1
                account_id = account.id
                models.db.session.delete(session)
                models.db.session.commit()
                models.db.session.close()
                response = jsonify(     message="Account Finalized Successfully",
                                        status=True,
                                        HTTP_CODE=200,
                                        account_id=account_id
                          )
                response.status_code = 200
            else:
                # Send Error
                response = jsonify(     message="Account Finalized Failure",
                                        status=False,
                                        HTTP_CODE=200
                          )
                response.status_code = 200
        else:
            # Send Error
            response = jsonify(     message="Account Finalized Failure",
                                    status=False,
                                    HTTP_CODE=200
                      )
            response.status_code = 200
    else:
        # Send Error
        response = jsonify(     message="Account Finalized Failure",
                                status=False,
                                HTTP_CODE=200
                      )
        response.status_code = 200
    return response


@auth.route("/account/signOn", methods=["POST"])
def sign_into_account():
    req = json.loads(request.form["params"].encode('utf8'))
    account_id = req["account_id"]

    session_id = uuid.uuid4().hex
    offset = functions.Auth().create_session_offset()
    session = models.ApiUserSession(session_id, account_id, offset)
    models.db.session.add(session)
    models.db.session.commit()

    models.db.session.close()

    response = jsonify(    message="Account Sign in Successful",
                           status=True,
                           HTTP_CODE=200,
                           account_id=account_id,
                           session_id=session_id,
                           offset=offset
                      )
    response.status_code = 200
    return response


@auth.route("/account/authenticate", methods=["POST"])
def authenticate_account():
    req = json.loads(request.form["params"].encode('utf8'))
    session_id = req["session_id"]
    password_hash = req["password_hash"]
    response = None
    if session_id is not None and password_hash is not None:
        session = models.ApiUserSession.query.filter_by(id=session_id).first()
        if session is not None:

            session_password = functions.Auth().create_password_hash(session_id, session.offset_key, session.api_user_id)
            if password_hash == session_password:
                # Authenticate Session
                session.state = 1
                account_id = session.api_user_id
                session_id = session.id
                models.db.session.commit()
                models.db.session.close()
                response = jsonify(     message="Account Authenticated Successfully",
                                        status=True,
                                        HTTP_CODE=200,
                                        account_id=account_id,
                                        session_id=session_id
                          )
                response.status_code = 200
            else:
                # Send Error
                response = jsonify(     message="Account Authenticated Failure",
                                        status=False,
                                        HTTP_CODE=200
                          )
                response.status_code = 200
        else:
            # Send Error
            response = jsonify(     message="Account Authenticated Failure",
                                    status=False,
                                    HTTP_CODE=200
                      )
            response.status_code = 200
    return response


@auth.route("/account/logout", methods=["POST"])
def logout_account():
    req = json.loads(request.form["params"].encode('utf8'))
    session_id = req["session_id"]
    password_hash = req["password_hash"]
    response = None
    if session_id and password_hash:
        session = models.ApiUserSession.query.filter_by(id=session_id).first()
        if session is not None:
            session_password = functions.Auth().create_password_hash(session_id, session.offset_key, session.api_user_id)
            if password_hash == session_password:
                # Delete Session
                models.db.session.delete(session)
                models.db.session.commit()
                models.db.session.close()
                response = jsonify(     message="Account Logout Successfully",
                                        status=True,
                                        HTTP_CODE=200
                          )
                response.status_code = 200
        else:
            # Send Error
            response = jsonify(     message="Account Logout Failure",
                                    status=False,
                                    HTTP_CODE=200
                        )
            response.status_code = 200
    else:
        # Send Error
        response = jsonify(     message="Account Logout Failure",
                                status=False,
                                HTTP_CODE=200
                    )
        response.status_code = 200
    return response