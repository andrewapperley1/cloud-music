__author__ = 'andrewapperley'

from .. import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask.ext.login import UserMixin
from .. import login_manager
from flask import current_app


class ApiUser(db.Model):
    __table_name__ = 'api_users'
    id = db.Column(db.String(100), primary_key=True, unique=True)
    state = db.Column(db.SmallInteger)

    def __init__(self, id):
        self.id = id
        self.state = 0

    def __repr__(self):
        return '<ApiUser %s, %i>' % self.id, self.state


class ApiUserSession(db.Model):
    __table_name__ = 'api_user_sessions'
    id = db.Column(db.String(100), primary_key=True, unique=True)
    api_user_id = db.Column(db.String(100), unique=True)
    offset_key = db.Column(db.Integer)
    state = db.Column(db.SmallInteger)

    def __init__(self, id, user_id, offset):
        self.id = id
        self.api_user_id = user_id
        self.offset_key = offset
        self.state = 0

    def __repr__(self):
        return '<ApiUserSession %s, %s, %i, %i>' % self.id, self.api_user_id, self.offset_key, self.state


class User(UserMixin, db.Model):
    __tablename__ = 'dashboard_users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password_hash = db.Column(db.String(128))

    @property
    def password(self):
        raise AttributeError('password is not readable')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @staticmethod
    def check_username(username):
        username = username.replace(" ", "")
        message = None
        check = True
        if User.query.filter_by(username=username).count() > 0 and len(username) > 0:
            check = False
            message = "That Username is already taken or incorrect"
        return check, message

    @staticmethod
    def check_pin_code(pin_code):
        check = True
        message = None
        if current_app.config['REGISTRATION_PIN_CODE'] != pin_code:
            message = "That PIN Code is incorrect"
            check = False
        return check, message

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return '<User %s>' % self.username

    def is_anonymous(self):
        return False

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))