__author__ = 'andrewapperley'

from . import models
from .. import db
from flask.ext.login import login_user, logout_user
import math
import random
from flask import current_app
import hashlib


class Auth(object):

    @staticmethod
    def login(data):
        username = data['username']
        password = data['password']
        user = models.User.query.filter_by(username=username).first()
        if user:
            if user.check_password(password):
                return login_user(user)
        return False

    @staticmethod
    def register(data):
        username = data['username']
        password = data['password']
        pin_code = data['pinCode']
        checks = True
        message = ""
        if not models.User.check_username(username):
            checks = False
        if checks and message == '':
            if not models.User.check_pin_code(pin_code):
                checks = False
        if checks:
            new_user = models.User(username, password)
            db.session.add(new_user)
            db.session.commit()
            db.session.close()
            return True
        else:
            return False

    @staticmethod
    def logout():
        return logout_user()

    @staticmethod
    def verify_api_session(session_id, account_id, api_key):
        session = models.ApiUserSession.query.filter_by(id=session_id, api_user_id=account_id).first()
        if session is not None and api_key == current_app.config['API_PUBLIC_KEY']:
            return True
        else:
            return False

    @staticmethod
    def create_password_hash(session_id, offset, account_id):
        account_id = str(account_id)
        session_id = str(session_id)
        shared_key = current_app.config['API_SHARED_KEY']
        shared_chunk_amount = current_app.config['API_SHARED_CHUNK_AMOUNT']
        if (offset + shared_chunk_amount) > len(shared_key):
            sub_key = shared_key[offset:]
            sub_key += shared_key[:(offset + shared_chunk_amount) - len(shared_key)]
        else:
            sub_key = shared_key[offset:(offset+shared_chunk_amount)]

        binary_string = ''.join(format(ord(i), 'b').zfill(8) for i in sub_key)

        password_hash = hashlib.sha256(binary_string+account_id+session_id).hexdigest()
        return password_hash

    @staticmethod
    def create_session_offset():
        return int(math.floor(random.random()*(len(current_app.config['API_SHARED_KEY'])-1)+1))