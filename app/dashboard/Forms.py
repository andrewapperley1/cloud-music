__author__ = 'andrewapperley'

from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, SubmitField, HiddenField
from wtforms.validators import InputRequired, Length


class LoginForm(Form):
    username = StringField('username', validators=[InputRequired(), Length(5, 64)])
    password = PasswordField('password', validators=[InputRequired()])
    login = SubmitField()


class RegisterForm(Form):
    username = StringField('username', validators=[InputRequired(), Length(5, 64)])
    pinCode = StringField('pinCode', validators=[InputRequired()])
    password = PasswordField('password', validators=[InputRequired(), Length(5, 64)])
    register = SubmitField()