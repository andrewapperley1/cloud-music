__author__ = 'andrewapperley'

from flask import session, redirect, url_for, current_app, jsonify, render_template, request
from . import dashboard
from .. import db
from ..auth import functions as auth_functions
from ..auth import models as auth_models
from Forms import LoginForm, RegisterForm
from flask.ext.login import login_required, current_user, AnonymousUserMixin
from flask import flash
# import urllib

@dashboard.route('/', methods=['POST', 'GET'])
def root_page():
    return redirect('/dashboard/login')


@dashboard.route('/logout', methods=['GET'])
@login_required
def logout():
    url = request.url
    if auth_functions.Auth().logout():
        url = "/dashboard/login"
    return redirect(url)

@dashboard.route('/login', methods=['GET', 'POST'])
def login_page():
    form = LoginForm()
    _next = str(request.values.get("next"))
    _currentUser = current_user
    if _currentUser.is_anonymous():
        _currentUser = None

    if (form.validate_on_submit() and auth_functions.Auth().login(form.data)) or _currentUser:
        url = "/dashboard/music"
        if _next != "None":
            url = _next
        return redirect(url)

    return render_template('login.html', form=form, _next=_next)


@dashboard.route('/register', methods=['GET', 'POST'])
def register_page():
    form = RegisterForm()
    message = None
    if form.data['username'] is not None:
        _message = auth_models.User.check_username(form.data['username'])[1]
        if _message is not None:
            message = _message
    if form.data['pinCode'] is not None:
        _message = auth_models.User.check_pin_code(form.data['pinCode'])[1]
        if _message is not None:
            message = _message
    if form.validate_on_submit() and message is None:
        if auth_functions.Auth().register(form.data):
            return redirect('/dashboard/login')
    if message is not None:
        flash(message)
    return render_template('register.html', form=form)


@dashboard.route('/music', methods=['GET'])
@login_required
def music_page():
    return render_template('music.html')


# def get_next_url(url):
#     __next = "?next="
#     _q_index = url.find(__next)
#     if _q_index != -1:
#         _url = url[_q_index+len(__next):]
#         return urllib.unquote_plus(_url)