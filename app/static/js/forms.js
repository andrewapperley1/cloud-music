/**
 * Created by andrewapperley on 14-12-03.
 */

$(".login-form-button").click(function() {
    var pass = $("#password");
    var pass_val = pass.val().replace(/ /g,'');
    if (pass_val != "") {
        pass.val( CryptoJS.SHA256( pass_val ) );
    }
});