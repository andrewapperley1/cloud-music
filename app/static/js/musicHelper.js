/**
 * Created by andrewapperley on 14-12-06.
 */

function createUploadDropZone() {
    Dropzone.autoDiscover = false;
    var dropZone = new Dropzone("#music-upload-container", {
            url: "/music/dashboard/upload",
            clickable: false,
            acceptedFiles: "audio/mpeg,.mp3",
            uploadMultiple: true,
            paramName: "music"
        });
    dropZone.on("addedfile", function(file) {

    });

    dropZone.on("uploadprogress", function(progress) {

    });
}

function getArtists() {
    $.getJSON("/music/dashboard/artists", function (data) {
        generateArtistBlocks(data['artists']);
    });
}

function generateArtistBlocks(artists) {
    var artistContainer = $("#music-artists-container");
    for (var i in artists) {
        var artist = artists[i];
        var artist_name_joined = artist['artist']['name'].replace(/[^A-Z0-9]+/ig, '_');
        artistContainer.append(
            "<li id='"+artist_name_joined+"' class='music-artist-container'>" +
                "<div class='music-artist-albums-container'></div>" +
                "<h3>"+artist['artist']['name']+"</h3>" +
                "<p>"+
                "<span class='fa fa-music'></span>"+artist['artist']['song_count']+" Song(s) "+
                "<span class='fa fa-circle'></span>"+artist['artist']['album_count']+" Album(s) "+
                "<span class='fa fa-clock-o'></span>"+parseInt(artist['total_time']/60)+" Minute(s)"+
                "</p>" +
            "</li>"
        );
        var albums = artist['albums'];
        while (albums.length < 4) {
            albums.push("noArt");
        }
        var albumContainer = artistContainer.children("#"+artist_name_joined).children(".music-artist-albums-container");
        for (var ii in albums) {
            if (ii > 3){break;}
            var album_id = albums[ii];
            var alpha = 1;
            var url = "/static/media/public/albums/"+album_id+".jpg";
            if (album_id == "noArt") {
                alpha = 0.07;
                url = "/static/imgs/"+album_id+".png";
            }

            albumContainer.append(
                "<img src='"+url+"' class='img-responsive' style='opacity: "+alpha+";'/>"
            );
        }
        albumContainer.append("<div class='clearfix'></div>");
    }
    artistContainer.append("<div class='clearfix'></div>");
}