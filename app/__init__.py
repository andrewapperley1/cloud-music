__author__ = 'andrewapperley'

from flask import Flask, request, jsonify
from config import config
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
from flask.ext.cors import CORS

db = SQLAlchemy()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = '/dashboard/login'
bootstrap = Bootstrap()


def create_app(config_env):
    app = Flask(__name__)
    cors = CORS(app)
    app.config.from_object(config[config_env])
    config[config_env].init_app(app)
    db.init_app(app)
    bootstrap.init_app(app)
    login_manager.init_app(app)
    # from .auth import init_app as auth_init
    # auth_init()

    from .music import music as music_blueprint
    from .music import models as music_models
    from .auth import models as auth_models
    from .auth import auth as auth_blueprint
    from .dashboard import dashboard as dashboard_blueprint

    app.register_blueprint(music_blueprint, url_prefix='/music')
    app.register_blueprint(dashboard_blueprint, url_prefix='/dashboard')
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    with app.app_context():
        db.create_all()

    return app